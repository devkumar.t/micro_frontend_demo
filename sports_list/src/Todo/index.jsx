import React, { useState } from "react";

const Todo = () => {
  const [input, setinput] = useState("");
  const [data, setdata] = useState([]);

  const addItem = () => {
    if (input?.length > 0) {
      let id = data?.length + 1;
      setdata((p) => [...p, { id: id, name: input }]);
      setinput("");
    }
  };

  const removeItem = (id) => {
    let filterData = data?.filter((x) => x?.id !== id);
    setdata(filterData);
  };

  return (
    <div className="container">
      <div className="list_container">
        <input
          onChange={(e) => {
            setinput(e.target.value);
          }}
          value={input}
          className="input"
          type="text"
        />
        <button onClick={addItem} className="button">

          Add

        </button>
      </div>
      <div className="list_view">
        {data?.length === 0 ? <div className="item">No Data Found</div> : null}
        {data?.map((x, i) => (
          <div key={i} className="item">
            <span>
             
              {++i}. {x?.name}
            </span>
            <button
              onClick={() => {
                removeItem(x?.id);
              }}
              className="remove_btn"
            >
              Remove
            </button>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Todo;
