import "./App.css";
import Todo from "./Todo";

function App() {
  return (
    <>
      <h2>Sports List</h2>
      <Todo />
    </>
  );
}

export default App;
