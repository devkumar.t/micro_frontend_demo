
import { useState, lazy,Suspense } from "react";
// import Todo from "sportList/Todo";
import "./App.css";
const Todo = lazy(() => import("sportList/Todo"));
const One = lazy(() => import("./one"));
import { ErrorBoundary } from "./Errorboundary";

function App() {
  return (
    <>
      <h2>Study List</h2>
      {/* <ErrorBoundary fallback={<div>Not found 404</div>}>
        <Suspense fallback={<div>Loading.....</div>}>
          <One />
        </Suspense>
      </ErrorBoundary> */}
      <ErrorBoundary fallback={<div>Not found 404</div>}>
      <Suspense fallback={<div>Loading.....</div>}>
        <Todo />
         </Suspense>
      </ErrorBoundary>


    </>
  );
}

export default App;
